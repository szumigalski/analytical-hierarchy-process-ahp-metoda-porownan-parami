const { User, Study } = require('../models') 
const createNamesLists = require('../utils/getNames')

// Controller get users list 
exports.getUserList = (req, res, next) => { 
    User.find({}, {}, (err, users) => { 
    if (err || !users) { 
      res.status(401).send({message: "Unauthorized"}) 
      next(err) 
    } else { 
      res.json({status: "success", users: users}); 
    } 
  })
}

exports.getUserStudies = (req, res, next) => { 
    let studiesList = []
    Study.find( { users: req.params.userId }, {}, (err, studies) => { 
    if (err || !studies) { 
      res.status(401).send({message: "Unauthorized"}) 
      next(err) 
    } else {
        studies.map(study => {
            studiesList.push({
                study: study,
                namesList: createNamesLists(study)
            })
        })
      res.json({status: "success", studies: studiesList}); 
    } 
  }) 
}

exports.deleteUser= async (req, res, next) => { 
    let user
    await User.findById(req.params.userId, function(err,obj) { 
        user = obj
    })
    if(user){
        return await User.findByIdAndDelete(req.params.userId, function (err, docs) {
        if (err){
            console.log(err)
        }
        else{
            console.log("User deleted : ", docs);
            res.json({status: "success"});
        }
    });
  }
}