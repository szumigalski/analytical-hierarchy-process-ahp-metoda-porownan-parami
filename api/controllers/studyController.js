const catchAsync = require('../utils/catchAsync');
const { studyService } = require('../services');
const httpStatus = require('http-status');
const pick = require('../utils/pick')
const ApiError = require('../utils/ApiError');


  const createStudy = catchAsync(async (req, res) => {
    const study = await studyService.createStudy(req.body);
    await studyService.addStudyToUser(req.body.users[0], study)
    //await studyService.addUserToStudy(study._id, { _id: req.body.users[0] })
    res.status(httpStatus.CREATED).send(study);
  });

  /*const createDays = catchAsync(async (req, res) => {
    console.log('create')
    const days = await dayService.createDays(req.body);
    res.status(httpStatus.CREATED).send(days);
  });*/

  const getStudies = catchAsync(async (req, res) => {
    const filter = pick(req.query, ['name', 'role']);
    const options = pick(req.query, ['sortBy', 'limit', 'page']);
    const result = await studyService.queryStudies(filter, options);
    res.send(result);
  });

  const getStudy = catchAsync(async (req, res) => {
    const study = await studyService.getStudyById(req.params.studyId);
    if (!study) {
      throw new ApiError(httpStatus.NOT_FOUND, 'Study not found');
    }
    res.send(study);
  });

  const updateStudy = catchAsync(async (req, res) => {
    const study = await studyService.updateStudyById(req.params._id);
    if (!study) {
      throw new ApiError(httpStatus.NOT_FOUND, 'Study not found');
    }
    res.send(study);
  });

  const deleteStudy = catchAsync(async (req, res) => {
    const study = await studyService.deleteStudyById(req.params.studyId);
    res.send(study);
  });


  const updateResult = catchAsync(async (req, res) => {
    const study = await studyService.updateResultById(req.params);
    if (!study) {
      throw new ApiError(httpStatus.NOT_FOUND, 'Study not found');
    }
    res.send(study);
  });

  const updateNamesList = catchAsync(async (req, res) => {
    const study = await studyService.updateStudyNamesList(req.params);
    if (!study) {
      throw new ApiError(httpStatus.NOT_FOUND, 'Study not found');
    }
    res.send(study);
  });

module.exports = {
    createStudy,
    getStudies,
    getStudy,
    updateStudy,
    deleteStudy,
    updateResult,
    updateNamesList
  };