var nodemailer = require('nodemailer');
let { mailPassword } = require('../config')
const { User, Study } = require('../models');
let { renderTableNumber, getTableLength, getAhpTable } = require('../utils/ahp')

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'szumifilmy@gmail.com',
      pass: mailPassword
    }
  });
  
  var mailOptions = {
    from: 'szumifilmy@gmail.com',
    to: 'pwysocka85@gmail.com', //pwysocka85@gmail.com
    subject: 'AHP wysłany formularz',
    text: '',
    html: ''
  };

exports.sendMail= async (req, res, next) => {
    await console.log('req.body', req.body, req.body.user)
    let user 
    user = await User.find({_id: req.body.user.id }).exec()
    study = await Study.find({ _id: user[0].studies[user[0].studies.length -1]}).exec()
    await console.log('study', study[0].marks)
    let superTable = ''
    await study[0].marks.map( (result, i) => {
      if(i !== 4) {
        let tableLength = getTableLength(result.namesList)
        let ahp = new Promise( resolve => resolve(getAhpTable(result, tableLength)))
        ahp.then(value => {
          console.log('bravo: ', value)
          superTable +='<table style="border: 1px solid black;border-collapse: collapse;">'
        tableLength.map(x => {
          superTable += '<tr style="border: 1px solid black">'
          superTable += '<th>' + result.namesList[x] + '</th>'
          tableLength.map(y => {
            superTable += ("<td style='border: 1px solid black; width: 15px'>" + renderTableNumber(x, y, tableLength.length, result.results) + "</td>")
          })
          superTable += '</tr>'
        })
        superTable += '</table></br>'
        console.log('why?', result)
        superTable += '<div>' + value.itemRankMetaMap.tree.cr.toFixed(3) + '</div></br>'
        })
      }
    })
    await setTimeout(() => {
       console.log('superTable', superTable)
      let mail = 'wysłano mail'
      superTable += 'Formularz został wysłany przez użytkownika: ' + req.body.user.name
      mailOptions.html = superTable
       transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            console.log(error);
            mail = 'error'
          } else {
            console.log('Email sent: ' + info.response);
          }
        });
      res.send(mail);
    }, 5000)
}