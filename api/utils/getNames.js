const createNamesLists = (study) => {
    let namesLists = []
    let i, j
    study && study.marks.map((mark, x) => {
        i = 0
        j = 1
        namesLists.push([])
        while(i < mark.namesList.length) {
            j = i + 1
            while(j < mark.namesList.length) {
                namesLists[x].push([mark.namesList[i], mark.namesList[j]])
                j += 1
            }
            i += 1
        }  
    })
    return namesLists
}

module.exports = createNamesLists;