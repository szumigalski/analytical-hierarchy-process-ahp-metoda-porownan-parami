let AHP = require('ahp')

const fullMarks = {
    '0': '1/9',
    '5': '1/8',
    '10': '1/7',
    '15': '1/6',
    '20': '1/5',
    '25': '1/4',
    '30': '1/3',
    '35': '1/2',
    '40': '1',
    '45': '2',
    '50': '3',
    '55': '4',
    '60': '5',
    '65': '6',
    '70': '7',
    '75': '8',
    '80': '9'
}

const fullMarksNumbers = {
    '0': 1/9,
    '5': 1/8,
    '10': 1/7,
    '15': 1/6,
    '20': 1/5,
    '25': 1/4,
    '30': 1/3,
    '35': 1/2,
    '40': 1,
    '45': 2,
    '50': 3,
    '55': 4,
    '60': 5,
    '65': 6,
    '70': 7,
    '75': 8,
    '80': 9
}

const revertResult = {
    '1/9': '9',
    '9': '1/9',
    '1/8': '8',
    '8': '1/8',
    '1/7': '7',
    '7': '1/7',
    '1/6': '6',
    '6': '1/6',
    '1/5': '5',
    '5': '1/5',
    '1/4': '4',
    '4': '1/4',
    '3': '1/3',
    '1/3': '3',
    '2': '1/2',
    '1/2': '2',
    '1': '1'
}

const renderTableNumber = (x, y, length, data) => {
    if (data) {
         if(x === y) return 1
         let pom = 0
         let count = 0
         if(y > x) {
             while(count < x) {
                 count += 1
                 pom += length - count
             }
             pom += y-x-1
             return fullMarks[data[pom]]
         }
         while(count < y) {
             count += 1
             pom += length - count
         }
         pom += x-y-1
         return revertResult[fullMarks[data[pom]]]
     }
 }

 const getTableLength = (dataNames) => {
    let newTable = []
    for(let i=0; i< dataNames.length; i+=1) {
        newTable.push(i)
    }
    return newTable
 }

 const ahpFunction = (names, data, withFull) => {
    let ahpContext = new AHP();
    ahpContext.addItems(names);
    ahpContext.addCriteria(['tree']);
    ahpContext.rankCriteriaItem('tree', getAhpData(names, data, withFull));
    let output = ahpContext.run();
    return output
}

const getAhpData = (names, data, withFull) => {
    let resList = []
    let counter = 0
    for(let i=0;i<names.length; i+=1) {
        for(let j=i+1; j< names.length; j+=1) {
            resList.push([names[i], names[j], withFull ? fullMarksNumbers[data[counter]] : data[counter]])
            counter+=1
        }
    }
    return resList
}

 const getAhpTable = async (data, tableLength) => {
    let ahpTable = await []
        await ahpTable.push(ahpFunction(data.namesList, data.results, true))
        return await ahpTable[0]
 }

 module.exports = {
    getTableLength,
    fullMarks,
    fullMarksNumbers,
    revertResult,
    renderTableNumber,
    getAhpTable
 }