const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const User = require('./User');

const marksSchema = mongoose.Schema({
  namesList: {
    type: Array
  },
  results: {
    type: Array,
    default: [] 
  },
  status: Boolean
})

const studySchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      index: true,
      unique: true
    },
    marks: [marksSchema],
    users: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: User
    }]
  }
);

studySchema.plugin(uniqueValidator, { message: 'Error, expected id to be unique.' })
const Study = mongoose.model('Study', studySchema);

module.exports = Study;