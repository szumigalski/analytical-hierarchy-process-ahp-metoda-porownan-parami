const { Study, User } = require('../models');
const ObjectId = require('mongoose').Types.ObjectId;
const createNamesLists = require('../utils/getNames')

const createStudy = async (studyBody) => {
    console.log('studyBody', studyBody)
    const study = await Study.create(studyBody);
    return study;
  };

const queryStudies = async (filter, options) => {
    const studies = await Study.find().populate('users')
    let studiesList = await []
    studies.map(study => {
      studiesList.push({
        study: study,
        namesList: createNamesLists(study)
      })
    })
    return studiesList;
  };

const getStudyById = async (id) => {
    return Study.find({_id: id});
  };

const deleteStudyById = async (id) => {
  let study
  await Study.findById(id, function(err,obj) { 
    console.log('obj', obj)
    study = obj
  })
  await console.log('study', study)
  if(study){
    return await Study.findByIdAndDelete(id, function (err, docs) {
      if (err){
          console.log(err)
      }
      else{
        study.users.map(user=> {
          console.log('user', user)
          deleteStudyFromUser(user, { _id: study._id})
        })
          console.log("Deleted : ", docs);
      }
  });
  }
  };

const updateStudyById = async (id, body) => {
    return Study.findOneAndUpdate({ _id: id}, body)
  };

const updateStudyNamesList = (params) => {
  return Study.update({ 'marks._id': params.markId}, {'$set': {
    ["marks.$.namesList." + params.listId]: params.value
  }})
}

const updateResultById = async (params) => {
    return Study.update({ 'marks._id': params.markId}, {'$set': {
      ["marks.$.results."+params.resultId]: params.value
    }})
  };

const addUserToStudy = function(studyId, user) {
    return Study.findByIdAndUpdate(
        studyId,
      { $push: { users: user._id } },
      { new: true, useFindAndModify: false }
    );
  };

const addStudyToUser = function(userId, study) {
    return User.findByIdAndUpdate(
        userId,
      { $push: { studies: study._id } },
      { new: true, useFindAndModify: false }
    );
  };

const deleteStudyFromUser = function(userId, study) {
    return User.findByIdAndUpdate(
        userId.toString(),
      { $pull: { studies: study._id } },
      { new: true, useFindAndModify: false }
    );
  };

module.exports = {
    createStudy,
    queryStudies,
    getStudyById,
    deleteStudyById,
    updateStudyById,
    addUserToStudy,
    addStudyToUser,
    deleteStudyFromUser,
    updateResultById,
    updateStudyNamesList
  };