const express = require('express');
const mailController = require('../controllers/mail') 

const router = express.Router();

router
  .post('/sendmail', mailController.sendMail)

module.exports = router;