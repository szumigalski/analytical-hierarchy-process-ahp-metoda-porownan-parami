const express = require('express');
const studyRoute = require('./studyRoute');
const userRoute = require('./userRoute')
const mailRoute = require('./mailRoute')

const router = express.Router();

router.use('/study', studyRoute);
router.use('/auth', userRoute);
router.use('/mail', mailRoute);

module.exports = router;