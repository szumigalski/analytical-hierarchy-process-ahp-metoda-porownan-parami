const express = require('express');
const authController = require('../controllers/auth') 
const studyController = require('../controllers/studyController');

const router = express.Router();

router
  .get('/studies', studyController.getStudies)
  .get('/study/:studyId', studyController.getStudy)
  .post('/study', authController.accessTokenVerify, studyController.createStudy)
  .put('/study/:studyId', authController.accessTokenVerify, studyController.updateStudy)
  .put('/study/:studyId/marks/:markId/result/:resultId/value/:value', authController.accessTokenVerify, studyController.updateResult)
  .put('/study/:studyId/marks/:markId/namesList/:listId/value/:value', authController.accessTokenVerify, studyController.updateNamesList)
  .delete('/study/:studyId', authController.accessTokenVerify, studyController.deleteStudy)

module.exports = router;
