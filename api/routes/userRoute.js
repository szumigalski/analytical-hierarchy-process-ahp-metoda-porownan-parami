const express = require('express') 
const authController = require('../controllers/auth') 
const usersController = require('../controllers/users') 

const router = express.Router(); 

router.post('/login', authController.loginUser); 
router.post('/refresh', authController.refreshTokenVerify); 

// secure router 
router.get('/users', authController.accessTokenVerify, usersController.getUserList); 
router.get('/user/:userId/studies', authController.accessTokenVerify, usersController.getUserStudies); 
router.post('/register', authController.accessTokenVerify, authController.createUser); 
router.delete('/user/:userId', authController.accessTokenVerify, usersController.deleteUser)
module.exports = router;