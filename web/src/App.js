import LoginComponent from './components/LoginComponent/LoginComponent'
import FormsComponent from './components/FormsComponent/FormsComponent'
import CheckComponent from './components/CheckComponent/CheckComponent'
import ThanksComponents from './components/ThanksComponent/ThanksComponent'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {
  return (
    <Router>
        <Switch>
          <Route path="/end">
            <ThanksComponents />
          </Route>
          <Route path="/forms">
            <FormsComponent />
          </Route>
          <Route path="/check">
            <CheckComponent />
          </Route>
          <Route path="/">
            <LoginComponent />
          </Route>
        </Switch>
    </Router>
  );
}

export default App;
