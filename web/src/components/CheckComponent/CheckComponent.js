import React, { useEffect, useState } from 'react'
import { Segment, Dropdown, Pagination, Checkbox, Form } from 'semantic-ui-react'
import { getUsers, getUserStudy } from './CheckApi'
import Menu from '../FormsComponent/MenuComponent'
import TableComponent from '../FormsComponent/TableComponent'
import { ahpFunction } from '../FormsComponent/ahpComponent'
import { fullMarksNumbers } from '../addons'

const CheckComponent = () => {
    let [study, setStudy] = useState(null)
    let [names, setNames] = useState(null)
    let [users, setUsers] = useState([])
    let [usersTable, setUsersTable] = useState([])
    let [ahp, setAhp] = useState(null)
    let [ahpMany, setAhpMany] = useState(null)
    let [site, setSite] = useState(0)
    let [checkedUsers, setCheckedUsers] = useState([])
    let [option, setOption] = useState(1)
    let [usersWithStudies, setUsersWithStudies] = useState(null)
    let [manyData, setManyData] = useState(null)

    const tablesOnSite = [[6],[1,0], [2],  [3],[5]]

    useEffect(async () => {
        let newUsers = await getUsers()
        let usersTable = await []
        let newCheckedUsers = await []
        await newUsers.map((u, i) => {
            usersTable.push({
                key: i,
                text: u.email,
                value: u._id
            })
            newCheckedUsers.push(true)
        })
        await setUsersTable(usersTable)
        await setCheckedUsers(newCheckedUsers)
        await setUsers(newUsers)
    }, [])

    useEffect(async () => {
        let studiesList = await []
        if(users.length) {
            await users.map(async user => {
                let newUser = await getUserStudy(user._id)
                await studiesList.push(newUser)
            })
            await setUsersWithStudies(studiesList)
        }
    }, [users.length, checkedUsers])

    useEffect(() => {
        let func = async () => {
            let ahpTableMany = await []
            if(manyData && study) {
                await tablesOnSite[site].map(el => {
                    ahpTableMany.push(ahpFunction(study.marks[el].namesList, manyData[el], false))
                })
            }
            await setAhpMany(ahpTableMany)
        }
        func()
    }, [manyData, site])

    let changeUser = async (val, id) => {
        let newStudy = await getUserStudy(id)
        setStudy(newStudy.study)
        setNames(newStudy.namesList)
        let ahpTable = []
        if(newStudy.study) {
            await tablesOnSite[site].map(el => {
                ahpTable.push(ahpFunction(newStudy.study.marks[el].namesList, newStudy.study.marks[el].results, true))
            })
            await setAhp(ahpTable)
            await setManyUsersData(checkedUsers)
        }
    }

    const changePage = (e, number) => {
        setSite(number.activePage-1)
    }

    useEffect(async ()=> {
        if(study){
            let ahpTable = []
            await tablesOnSite[site].map(el => {
                ahpTable.push(ahpFunction(study.marks[el].namesList, study.marks[el].results, true))
            })
            await setAhp(ahpTable)
        }
    }, [study, site])

    const setManyUsersData = (newChecked) => {
        let newData = []
        let minUser = 0
        let count = 0
        while(!newChecked[minUser] && minUser < usersWithStudies.length) {
            minUser += 1
        }
        for(let i=0; i<newChecked.length; i+=1) {
            newChecked[i] && (count+=1)
        }
        usersWithStudies.map((user, i) => {
            newChecked[i] && user.study.marks.map((mark, j) => {
                let newMarks = []
                mark.results.map(res => {
                    newMarks.push(parseFloat(fullMarksNumbers[res]))
                })
                if (i === minUser) {
                    newData.push(newMarks) 
                } else {
                    newData[j] = addTables(newMarks, newData[j])
                }
            })
        })
        newData.map((item, i) => {
            item.map((x, j) => {
                newData[i][j] = (x/count).toFixed(3)
            })
        })
        setManyData(newData)
    }

    let addTables = (a,b) => {
        let newt = []
        a.map((v, i) => {
            newt.push(v + b[i])
        })
        return newt
    }

    return(<div>
        <Menu />
        <Form style={{ display: 'flex', justifyContent: 'space-around', width: '40%', flexFlow: 'wrap', marginLeft: 20 }}>
            <Form.Field>
            <Checkbox
                radio
                label='Wektory i CI'
                name='checkboxRadioGroup'
                value={1}
                checked={option === 1}
                onChange={() => setOption(1)}
            />
            </Form.Field>
            <Form.Field>
            <Checkbox
                radio
                label='Macierz Saati`ego'
                name='checkboxRadioGroup'
                value={2}
                checked={option === 2}
                onChange={() => setOption(2)}
            />
            </Form.Field>
            <Form.Field>
            <Checkbox
                radio
                label='Transponowana Macierz Saati`ego'
                name='checkboxRadioGroup'
                value={3}
                checked={option === 3}
                onChange={() => setOption(3)}
            />
            </Form.Field>
            <Form.Field>
            <Checkbox
                radio
                label='Macierz trójkątna'
                name='checkboxRadioGroup'
                value={4}
                checked={option === 4}
                onChange={() => setOption(4)}
            />
            </Form.Field>
        </Form>
        <div style={{display: 'flex'}}>
            <Segment style={{width: '40%', margin: 20}}>
                <Dropdown
                    placeholder='Wybierz osobę'
                    fluid
                    selection
                    onChange={(pom, user) => changeUser(pom, user.value)}
                    options={usersTable}
                />
                {study && ahp && tablesOnSite[site].map((element, x) =>
                    <TableComponent 
                        dataNames={study.marks[element].namesList}
                        data={study.marks[element].results}
                        ahp={ahp[x]}
                        option={option} 
                    />)}
            </Segment>
            <Segment style={{width: '40%', margin: 20}}>
                {users && users.map((user, i) => 
                    <Checkbox 
                        label={user.email} 
                        style={{ marginRight: 20}} 
                        checked={checkedUsers[i]}
                        onChange={async() => {
                            let newChecked = await [...checkedUsers]
                            newChecked[i] = await !newChecked[i]
                            await setCheckedUsers(newChecked)
                            if(usersWithStudies && usersWithStudies.length) {
                                await setManyUsersData(newChecked)
                            }
                        }}
                        />)}
                {study && ahp && manyData && tablesOnSite[site].map((element, x) =>
                    <TableComponent 
                        dataNames={study.marks[element].namesList}
                        data={manyData[element]}
                        ahp={ahpMany[x]}
                        option={6} 
                    />)}
            </Segment>
        </div>
        <div style={{display: 'flex', justifyContent: 'center', margin: 20}}>
                <Pagination 
                    defaultActivePage={5} 
                    totalPages={tablesOnSite.length} 
                    activePage={site+1}
                    onPageChange={async (e, number) => {
                        await changePage(e, number)
                        }} />
            </div>
    </div>)
}

export default CheckComponent