import axios from 'axios'
import { address } from '../../config'

const config = {
    headers: { 
        "Authorization": `Bearer ${localStorage.getItem('ahpAccessToken')}`,
        'Content-Type': 'application/x-www-form-urlencoded' 
    }
};

export const getUsers = async () => {
    const data = await axios.get(`${address}/auth/users`, config)
        .then(res => { 
            return res.data.users
        })
        .catch(err => {
            console.log('err: ', err)
        })
    return data
}

export const getUserStudy = async (id) => {
    const data = await axios.get(`${address}/auth/user/${id}/studies`, config)
        .then(res => { 
            return res.data.studies[0]
        })
        .catch(err => {
            console.log('err: ', err)
        })
    return data
}