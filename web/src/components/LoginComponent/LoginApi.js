import { address } from '../../config'
import axios from 'axios'
import { toast } from 'react-semantic-toasts';

export const loginUser = (email, password, isValid) => {
    axios.post(`${address}/auth/login`, {email, password})
        .then(res => {
            localStorage.setItem('ahpAccessToken', res.data.accessToken);
            localStorage.setItem('ahpRefreshToken', res.data.refreshToken);
            localStorage.setItem('ahpUser', res.data.userId);
            localStorage.setItem('ahpUserName', email);
            isValid(true)
            toast(
                {
                    title: 'Sukces',
                    type: 'success',
                    description: <p>Zalogowano poprawnie</p>
                })
        })
        .catch(err => {
            console.log(err)
            toast(
                {
                    title: 'Błąd',
                    type: 'error',
                    description: <p>Login lub hasło są niepoprawne</p>
                })
        })
}