import React, {useState, useEffect} from 'react'
import {
    Segment,
    Form,
    Button
} from 'semantic-ui-react'
import {
    useHistory
  } from "react-router-dom";
import { loginUser } from './LoginApi'

const LoginComponent =()=> {
    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')
    const [valid, isValid] = useState(false)
    const [page, setPage] = useState('/')
    const history = useHistory();

    useEffect(() => {
        if(valid) {
            history.push(page)
        }
    }, [valid])

    return(
    <div style={{height: '100vh', display: 'flex', justifyContent:'center', backgroundColor: '#fcf5f5'}}>
        <Segment style={{width: 300, margin: 'auto'}}>
            <Form>
                <Form.Field>
                    <label>Login</label>
                    <input 
                        onChange={(e)=> setLogin(e.target.value)}
                        value={login}
                        placeholder='Login...' 
                        />
                </Form.Field>
                <Form.Field>
                    <label>Hasło</label>
                    <input 
                        onChange={(e)=> setPassword(e.target.value)}
                        value={password}
                        placeholder='Hasło...' 
                        />
                </Form.Field>
                <Button type='submit' onClick={async() => {
                    await setPage('/forms')
                    await loginUser(login, password, isValid)}}> 
                    Zaloguj
                </Button>
            </Form>
        </Segment>
    </div>
    )
}

export default LoginComponent