import React from 'react'
import {
    Segment,
    Button,
    Icon
} from 'semantic-ui-react'
import {
    useHistory
  } from "react-router-dom";

const LoginComponent =()=> {
    const history = useHistory();

    return(
    <div style={{height: '100vh', display: 'flex', justifyContent:'center', backgroundColor: '#fcf5f5'}}>
        <Segment style={{width: 300, margin: 'auto', padding: 50}}>
            <Icon name='check circle outline' style={{ display: 'block', margin: 'auto', marginBottom: 20 }} size='massive' color='green'></Icon>
            Dziękujemy za Twój poświęcony czas
            <Button onClick={() => history.push('/')} style ={{ marginTop: 20 }}>Wróć do panelu logowania</Button>
        </Segment>
    </div>
    )
}

export default LoginComponent