import axios from 'axios'
import { address } from '../../config'

const config = () => {
    console.log('token', localStorage.getItem('ahpAccessToken'))
    return {headers: { 
        "Authorization": `Bearer ${localStorage.getItem('ahpAccessToken')}`,
        'Content-Type': 'application/x-www-form-urlencoded' 
    }}
};

const configPost = {
    headers: { 
        "Authorization": `Bearer ${localStorage.getItem('ahpAccessToken')}`,
        'Content-Type': 'application/json' 
    }
};

export const getStudy = async (id) => {
    const data = await axios.get(`${address}/auth/user/${id}/studies`, config())
        .then(res => {
            let i = 0
            while(res.data.length === 0) {
                i+=1
            } 
            return res.data.studies[i]
        })
        .catch(err => {
            console.log('err: ', err)
        })
    return data
}

export const updateResult = async (val, mark, number, studyId) => {
    const data = await axios.put(`${address}/study/study/${studyId}/marks/${mark}/result/${number}/value/${val}`,{}, config())
        .then(res => { 
            return res.data
        })
        .catch(err => {
            console.log('err: ', err)
        })
    return data
}

export const sendMail = async () => {
    let datasend = { 
        user: {
            name: localStorage.getItem('ahpUserName'),
            id: localStorage.getItem('ahpUser')
        }
    }
    const data = await axios.post(`${address}/mail/sendmail`,datasend, configPost)
        .then(res => { 
            return res.data
        })
        .catch(err => {
            console.log('err: ', err)
        })
    return data
}