import AHP from 'ahp'
import { fullMarksNumbers} from '../addons'

export const ahpFunction = (names, data, withFull) => {
    let ahpContext = new AHP();
    ahpContext.addItems(names);
    ahpContext.addCriteria(['tree']);
    ahpContext.rankCriteriaItem('tree', getAhpData(names, data, withFull));
    let output = ahpContext.run();
    return output
}

const getAhpData = (names, data, withFull) => {
    let resList = []
    let counter = 0
    for(let i=0;i<names.length; i+=1) {
        for(let j=i+1; j< names.length; j+=1) {
            resList.push([names[i], names[j], withFull ? fullMarksNumbers[data[counter]] : data[counter]])
            counter+=1
        }
    }
    return resList
}