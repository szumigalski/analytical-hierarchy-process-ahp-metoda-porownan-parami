import React from 'react'
import {Menu, Button} from 'semantic-ui-react'
import {
  useHistory
} from "react-router-dom";

const MenuComponent = ({navigation}) => {
    const history = useHistory();

    return(<Menu size='large' color='blue'>
    <Menu.Item
      content='AHP Metoda porównań parami'
    />
    <Menu.Item position='right'>
        <Button primary onClick={()=>{
          localStorage.removeItem('ahpAccessToken')
          localStorage.removeItem('ahpRefreshToken')
          localStorage.removeItem('ahpUser')
          history.push('/')
        }}>Wyloguj</Button>
    </Menu.Item>

  </Menu>)
}

export default MenuComponent