import React, { useEffect, useState } from 'react'
import { Segment, Pagination, Icon, Button, Header } from 'semantic-ui-react'
import Menu from './MenuComponent'
import SliderItem from './SliderItemComponent'
import { getStudy, sendMail } from './FormsApi'
import { ahpFunction } from './ahpComponent'
import {fullMarks, revertResult} from '../addons'
import {
    useHistory
  } from "react-router-dom";
import moment from 'moment'

const FormsComponent = ({navigation}) => {
    let startTime = 1000*5*60
    const [study, setStudy] = useState(null)
    const [names, setNames] = useState(null)
    const [ahp, setAhp] = useState(null)
    const [site, setSite] = useState(0)
    const [maxProblem, setMaxProblem] = useState(null)
    const [cr, setCr] = useState(null)
    const [check, setCheck] = useState([false, false, false, false])
    const [time, setTime] = useState(startTime)
    const [pause, setPause] = useState(true)

    const tablesOnSite = [[1,0],[2],[3],[5]]
    const headersOfTables = [
        [
            "Wskaż, które korzyści siedliskowe, dostarczane przez drzewa i krzewy, są ważniejsze i o ile",
            "Wskaż, które korzyści zaopatrujące, dostarczane przez drzewa i krzewy, są ważniejsze i o ile"
        ],
        [
            "Wskaż, które korzyści regulacyjne, dostarczane przez drzewa i krzewy, są ważniejsze i o ile"
        ],
        [
            "Wskaż, które korzyści kulturowe, dostarczane przez drzewa i krzewy, są ważniejsze i o ile"
        ],
        [
            "Wskaż, która grupa korzyści dostarczanych przez drzewa i krzewy jest ważniejsza i o ile"
        ]
    ]

    const tablesOnSiteWithMess = [[],[1,0], [2],[],  [3],[5], []]

    const history = useHistory();

    useEffect(async ()=> {
        let newStudy = await null
        newStudy = await getStudy(localStorage.getItem('ahpUser'))
        if(!newStudy) {
            await setTimeout(async () => {
                await console.log('token', localStorage.getItem('ahpUser'))
                await getStudy(localStorage.getItem('ahpUser'))
                await setStudy(newStudy.study)
                await setNames(newStudy.namesList)
                let ahpTable = []
                let crTable = []
                if(newStudy.study && site !== 0 && site !== 3 && site !== 6) {
                    await tablesOnSite[convertSiteNumber(site)].map((el, i) => {
                        ahpTable.push(ahpFunction(newStudy.study.marks[el].namesList, newStudy.study.marks[el].results, true))
                        crTable.push(ahpTable[i].itemRankMetaMap.tree.cr.toFixed(3))
                    })
                await setAhp(ahpTable)
                await setCr(crTable)
            }
            }, 2000)
        } else {
            await setStudy(newStudy.study)
            await setNames(newStudy.namesList)
            let ahpTable = []
            let crTable = []
            if(newStudy.study && site !== 0 && site !== 3 && site !== 6) {
                await tablesOnSite[convertSiteNumber(site)].map((el, i) => {
                    ahpTable.push(ahpFunction(newStudy.study.marks[el].namesList, newStudy.study.marks[el].results, true))
                    crTable.push(ahpTable[i].itemRankMetaMap.tree.cr.toFixed(3))
                })
                await setAhp(ahpTable)
                await setCr(crTable)
            }
        }
    }, [])
    let timer
    useEffect(() => { 
        if(site === 3) {
            setPause(true)
            let i = 1
            timer = setInterval(() => {
                if(i < startTime/1000) {
                    i+=1
                } else {
                    setPause(false)
                }
                console.log('time: ', time)
                setTime(time-1000*i)
            }, 1000)
        } else {
            timer && clearInterval(timer)
            setTime(startTime)
        }
        return () => {
            timer && clearInterval(timer)
            setTime(startTime)
        };
    }, [site])



    useEffect(async ()=> {
        if(study && study.marks && site !== 0 && site !== 3 && site !== 6){
            let ahpTable = []
            let crTable = []
            await tablesOnSite[convertSiteNumber(site)].map((el, i) => {
                ahpTable.push(ahpFunction(study.marks[el].namesList, study.marks[el].results, true))
                crTable.push(ahpTable[i].itemRankMetaMap.tree.cr.toFixed(3))
            })
            await setAhp(ahpTable)
            await setCr(crTable)
        }
    }, [study, site])

    const changePage = (e, number) => {
        setSite(number.activePage-1)
    }

    useEffect(() => {
        if(site !== 0 && site !== 3 && site !== 6) {
            let maxTable = []
            tablesOnSite[convertSiteNumber(site)].map((element, x) => {
                let rank = []
                let saatyTable = []
                let saaty2 = []
                ahp && ahp[x] && Object.keys(ahp[x].rankedScoreMap).map(key => {
                    rank.push(ahp[x].rankedScoreMap[key])
                })
                if(rank.length){
                    for(let i=0; i<rank.length; i+=1) {
                        saatyTable.push([])
                        for(let j=0; j<rank.length; j+=1) {
                            saatyTable[i].push((rank[i]/rank[j]).toFixed(3))
                        }
                    }
                    for(let i=0; i<rank.length; i+=1) {
                        saaty2.push([])
                        for(let j=0; j<rank.length; j+=1) {
                            saaty2[i].push((eval(renderTableNumber(i, j, study.marks[element].namesList.length, study.marks[element].results))*rank[j]/rank[i]).toFixed(3))
                        }
                    }
                }
                let triangleSaaty = JSON.parse(JSON.stringify(saaty2))
                let max = 0
                let xx
                let xy
                for(let i = 0; i<rank.length; i+=1) {
                    for(let j=i+1; j<rank.length; j+=1) {
                        if(saaty2[i][j] < saaty2[j][i]) {
                            if(parseFloat(saaty2[j][i]) > max) {
                                max = parseFloat(saaty2[j][i])
                                xx = j
                                xy = i
                            }
                            triangleSaaty[i][j] = triangleSaaty[j][i]
                        } else {
                            if(max < parseFloat(saaty2[i][j])) {
                                max = parseFloat(saaty2[i][j])
                                xx = i
                                xy = j
                            }
                        }
                        triangleSaaty[j][i] = 'x'
                    }
                }
                study && maxTable.push(getResultNumber(xx, xy, study.marks[element].namesList.length))
                study && setTimeout(() => {
                    setMaxProblem(maxTable)
                },10)
            })
        }
    }, [ahp])

    let getResultNumber = (x, y, length) => {
        let pom = 0
        let count = 0
        if(y > x) {
            while(count < x) {
                count += 1
                pom += length - count
            }
            pom += y-x-1
        } else {
            while(count < y) {
                count += 1
                pom += length - count
            }
            pom += x-y-1
        }
        return pom
    }

    const renderTableNumber = (x, y, length, data) => {
        if(x === y) return 1
        let pom = 0
        let count = 0
        if(y > x) {
            while(count < x) {
                count += 1
                pom += length - count
            }
            pom += y-x-1
            return fullMarks[data[pom]]
        }
        while(count < y) {
            count += 1
            pom += length - count
        }
        pom += x-y-1
        return revertResult[fullMarks[data[pom]]]
    }

    const startPage = () => <Segment style = {{ margin: 'auto', padding: 50, width: 600 }}>
        Za chwilę przystąpisz do badania. Poprosimy o ustalenie znaczenie poszczególnych korzyści dostarczanych przez 
        drzewa i krzewy. Nim przejdziesz dalej, upewnij się, że zapoznałeś/-aś się dobrze z przesłanymi <a href='https://docs.google.com/document/d/1he9XOa-yzWXOpTl3ep7PAr1c2dLCD1kzdHQtviV16XY/edit?usp=sharing' target='_blank'>ogólnymi 
        informacjami o badaniu</a> (sugerujemy, by mieć je otwarte w drugiej karcie przeglądarki w trakcie wypełniania 
        kwestionariusza) oraz <a href='https://youtu.be/MUqhubzGTJM' target='_blank'>instrukcją</a>. <br/> <br/>
        Badanie składa się z 4 stron z porównaniami. Kolejne uruchamiają się po naciśnięciu przycisku “przejdź dalej”. 
        W każdej chwili można się też cofnąć do poprzedniej strony (przycisk “cofnij”). W połowie sugerujemy zrobić 
        krótką przerwę. Prosimy nie zapomnieć nacisnąć przycisku “wyślij” po zakończeniu wypełniania. W razie problemów, 
        prosimy kontaktować się z Patrycją Przewoźną pod numerem telefonu <b>608172288</b>.
        </Segment>
    const getTimer = () => {
        let timeString = ''
        moment.duration(time).minutes() < 10 && (timeString+='0')
        timeString += moment.duration(time).minutes()
        timeString += ' : '
        moment.duration(time).seconds() < 10 && (timeString += '0')
        timeString += moment.duration(time).seconds()
        return timeString
    }
    const middlePage = () => <Segment style = {{ margin: 'auto', padding: 50, width: 600 }}>
        Nie jest dobrze podejmować zbyt wielu decyzji na raz. Ponieważ jesteś dokładnie w połowie badania sugerujemy, 
        byś zrobił/-a sobie choć krótką przerwę, nim wypełnisz drugą część kwestionariusza.
        <div>{getTimer()}</div>
    </Segment>

    const endPage = () => 
        <Segment style = {{ margin: 'auto', padding: 50, width: 600, display: 'flex', flexDirection: 'column' }}>
            Czy chcesz zakończyć badanie i przesłać wyniki? Jeśli tak, naciśnij “wyślij”, jeśli chcesz coś poprawić, naciśnij “cofnij”.
            <Button style={{ marginTop: 50}} onClick={() => {
                history.push('/end')
                sendMail()
            }}>Wyślij</Button>
        </Segment>

    const convertSiteNumber = (x) => {
        if (x < 3) return x - 1
        return x -2
    }

    return(
        <div style={{ backgroundColor: '#fcf5f5', minHeight: '100vh'}}>
            <Menu navigation={navigation} />
            {site === 0 ? startPage() :
                site === 3 ? middlePage() :
                site === 6 ? endPage() :
                tablesOnSiteWithMess[site] && tablesOnSiteWithMess[site].map((element, x) => 
                <div style={{ display: 'flex', justifyContent: 'center' }} key={x}>
                    <Segment style={{width: 1000, margin: '0 20px', height: '100%'}}>
                        <Header as='h3' style={{marginBottom: 30}}>{headersOfTables[convertSiteNumber(site)][x]}</Header>
                        {study && names && study.marks && study.marks[element].results.map((result, j) =>
                                <div style={{
                                    marginBottom: 40, 
                                    border: check[convertSiteNumber(site)] && maxProblem && maxProblem[x] === j && cr && cr[x] && cr[x] > 0.1 ? '4px solid red' : 'none',
                                    padding: check[convertSiteNumber(site)] && maxProblem && maxProblem[x] === j && cr && cr[x] && cr[x] > 0.1 ? 15 : 0, 
                                    borderRadius: 10
                                    }} key={j}>
                                    <SliderItem 
                                        names={names[element][j]} 
                                        value={result} 
                                        mark={study.marks[element]._id} 
                                        number={j} 
                                        setStudy={setStudy}
                                        studyId={study._id} />
                                        {check[convertSiteNumber(site)] && maxProblem && maxProblem[x] === j && cr && cr[x] && cr[x] > 0.1 &&
                                        <div style={{ color: 'red', marginTop: 40 }}><Icon color='red' name='warning sign' />
                                            <b>Wartość powodująca największą niespójność! {result > 40 ? '<--' : '-->'}</b>
                                        </div>}
                                </div>
                                )
                        }
                    </Segment>
                </div>
            )}
            {site !== 0 && site !== 3 && site !== 6 &&<div style={{display: 'flex', justifyContent: 'center', marginTop: 20}}>
                <Button onClick={() => {
                    let newCheck = [...check]
                    newCheck[convertSiteNumber(site)] = true
                    setCheck(newCheck)
                }}>SPRAWDŹ</Button>
            </div>}
            {check[convertSiteNumber(site)] && tablesOnSite[convertSiteNumber(site)].map((element, x) => 
                    <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: 20}}>
                        <div>{headersOfTables[convertSiteNumber(site)][x]}:</div>
                        {cr && cr[x] && cr[x] > 0.1 
                            ? <b style={{color: 'red'}}>Odpowiedzi nie są spójne!</b>:<b style={{color: 'green'}}>Odpowiedzi są spójne</b>}
                    </div>
                )}
            <div style={{display: 'flex', justifyContent: 'center', margin: 20}}>
            {site > 0 &&<Button onClick={() => {setSite(site-1)}}>Cofnij</Button>}
            {site < 6 &&<Button onClick={() => {setSite(site+1)}} disabled={pause && site === 3}>Przejdź dalej</Button>}
            </div>
        </div>)
}

export default FormsComponent