import React, { useEffect, useState } from 'react'
import { Table } from 'semantic-ui-react'
import {fullMarks, revertResult} from '../addons'

const TableComponent = ({ data, dataNames, ahp, option}) => {
    let [tableLength, setTableLength] = useState([])
    let [saaty, setSaaty] = useState(null)
    let [saatyTrans, setSaatyTrans] = useState(null)
    let [triangleSaaty, setTriangleSaaty] = useState(null)

    useEffect(() => {
        if(dataNames){
            let newTable = []
            for(let i=0; i< dataNames.length; i+=1) {
                newTable.push(i)
            }
            setTableLength(newTable)
        }
    }, [dataNames, data])

    useEffect(() => {
        let rank = []
        let saatyTable = []
        let saaty2 = []
        ahp && Object.keys(ahp.rankedScoreMap).map(key => {
            rank.push(ahp.rankedScoreMap[key])
        })
        if(rank.length){
            for(let i=0; i<rank.length; i+=1) {
                saatyTable.push([])
                for(let j=0; j<rank.length; j+=1) {
                    saatyTable[i].push((rank[i]/rank[j]).toFixed(3))
                }
            }
            for(let i=0; i<rank.length; i+=1) {
                saaty2.push([])
                for(let j=0; j<rank.length; j+=1) {
                    saaty2[i].push((eval(renderTableNumber(i, j, tableLength.length, data))*rank[j]/rank[i]).toFixed(3))
                }
            }
        }
        let triangleSaaty = JSON.parse(JSON.stringify(saaty2))
        for(let i = 0; i<rank.length; i+=1) {
            for(let j=i+1; j<rank.length; j+=1) {
                if(saaty2[i][j] < saaty2[j][i]) {
                    triangleSaaty[i][j] = triangleSaaty[j][i]
                }
                triangleSaaty[j][i] = 'x'
            }
        }
        setSaaty(saatyTable)
        setSaatyTrans(saaty2)
        setTriangleSaaty(triangleSaaty)
    }, [ahp])

    const names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I']

    const renderTableNumber = (x, y, length, data) => {
       if (data) {
            if(x === y) return 1
            let pom = 0
            let count = 0
            if(y > x) {
                while(count < x) {
                    count += 1
                    pom += length - count
                }
                pom += y-x-1
                return fullMarks[data[pom]]
            }
            while(count < y) {
                count += 1
                pom += length - count
            }
            pom += x-y-1
            return revertResult[fullMarks[data[pom]]]
        }
    }

    const renderTableNumberOnly = (x, y, length, data) => {
        if (data) {
            if(x === y) return 1
            let pom = 0
            let count = 0
            if(y > x) {
                while(count < x) {
                    count += 1
                    pom += length - count
                }
                pom += y-x-1
                return data[pom]
            }
            while(count < y) {
                count += 1
                pom += length - count
            }
            pom += x-y-1
            return data[pom] !== "0.000" ? (1 / data[pom]).toFixed(3) : 0
        }
    }

    let setValue = (x, y) => {
        switch(option) {
            case 1:
                return renderTableNumber(x, y, tableLength.length, data)
            case 2:
                return saaty[x] ? saaty[x][y] : 'null'
            case 3:
                return saatyTrans[x] ? saatyTrans[x][y] : 'null'
            case 4:
                return triangleSaaty[x] ? triangleSaaty[x][y] : 'null'
            case 5:
                return saaty[x] ? (eval(renderTableNumber(x, y, tableLength.length, data)) - saaty[x][y]).toFixed(3) : 'null'
            case 6:
                return renderTableNumberOnly(x, y, tableLength.length, data)
        }
    }
    return(<Table definition celled>
        <Table.Header>
            <Table.HeaderCell />
            {tableLength.map(i => <Table.HeaderCell>{names[i]}</Table.HeaderCell>)}
            {(option === 1 || option === 6) &&<Table.HeaderCell>Vector</Table.HeaderCell>}
        </Table.Header>
        <Table.Body>
            {tableLength.map(x => 
                <Table.Row>
                    <Table.Cell>{dataNames[x]}</Table.Cell>
                    {tableLength.map(y => triangleSaaty.length && saatyTrans.length &&<Table.Cell>{setValue(x, y)}</Table.Cell>)}
                    {(option === 1 || option === 6) &&<Table.Cell>{ahp && ahp.rankedScoreMap && ahp.rankedScoreMap[dataNames[x]] && ahp.rankedScoreMap[dataNames[x]].toFixed(3)}</Table.Cell>}
                </Table.Row>
            )}
            {(option === 1 || option === 6) &&<Table.Row>
            <Table.Cell>CR:</Table.Cell>
            <Table.Cell>{ahp && ahp.itemRankMetaMap && ahp.itemRankMetaMap.tree.cr.toFixed(3)}</Table.Cell>
            </Table.Row>}
        </Table.Body>
    </Table>)
}

export default TableComponent