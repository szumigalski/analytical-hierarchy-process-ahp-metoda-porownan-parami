import React, {useState, useEffect} from 'react'
import { Range } from 'rc-slider';
import 'rc-slider/assets/index.css';
import { getStudy, updateResult } from './FormsApi'
import {marks} from '../addons'

const SliderItemComponent = ({value, mark, number, studyId, names, setStudy}) => {
    const [key, setKey] = useState(new Date())

    useEffect(() => {
        setKey(new Date())
    }, [value])

    const handleChange = async (val) =>{
        await updateResult(val, mark, number, studyId)
        let newStudy = await getStudy(localStorage.getItem('ahpUser'))
        console.log('newStudy', newStudy)
        await setStudy(newStudy.study)
    }

    return(
    <div style={{width: '100%'}}>
    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end', width: '100%', marginBottom: 5 }}>
        <div style={{color: 'red', width: '50%'}}>{names[1]}</div>
        <div style={{color: 'blue', width: '50%', textAlign: 'right'}}>{names[0]}</div>
    </div>
    <Range
        count={3}
        defaultValue={[value]}
        pushable
        key={key}
        marks={marks}
        max={80}
        step={10}
        onChange={(e) => handleChange(e)}
        trackStyle={[{ backgroundColor: 'green' }]}
        handleStyle={[{ backgroundColor: 'green' },]}
        railStyle={{ display: 'none' }}
        dotStyle={{borderColor: 'grey', backgroundColor: 'grey', width: '6px', height: '6px'}}
      /></div>)
}

export default SliderItemComponent