# Potrzebne materiały
- Link do prezentacji: [link](https://docs.google.com/presentation/d/11QUwHmE-gVUbb0w6eZO7cmm0x0BaOo8caeXoRkBKLbU/edit?usp=sharing)  
- Link do artykułu który w bardzo przystępny sposób opisuje całą procedurę obliczeniową do momentu obliczenia współczynnika CR na konkretnym przykładzie [link](http://rad.ihu.edu.gr/fileadmin/labsfiles/decision_support_systems/lessons/ahp/AHP_Lesson_1.pdf)  
- pakiet R - może okaże się  pomocny na etapie konstruowania narzędzia [link](https://cran.r-project.org/web/packages/ahpsurvey/vignettes/my-vignette.html)  
- pakiet ahp dla javascript - [link](https://www.npmjs.com/package/ahp)
  
W arkuszu interesują nas "indywidualne preferencje". Ważny rozdział "dealing with inconsistent and missing data" - gdzie jest opisane, jak można wytypować najbardziej niespójne pary porównywane w macierzy.

